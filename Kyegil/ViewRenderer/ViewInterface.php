<?php
/**
 * Part of ViewRenderer
 * Created by Kyegil
 * Date: 28/06/2020
 * Time: 21:53
 */

namespace Kyegil\ViewRenderer;


interface ViewInterface
{
    /**
     * @return string
     */
    public function __toString();

    /**
     * @param ViewInterface|null $parent
     * @return ViewInterface
     */
    public function setParent(?ViewInterface $parent = null): ViewInterface;

    /**
     * @return ViewInterface|null
     */
    public function getParent(): ?ViewInterface;
}