<?php

namespace Kyegil\ViewRenderer;


/**
 * Class JsArray
 * @package Kyegil\ViewRenderer
 */
class JsArray extends ViewArray
{
    /**
     * @var string
     */
    protected $glue = ', ';

    /**
     * @var string
     */
    protected $prefix = '[';

    /**
     * @var string
     */
    protected $suffix = ']';
}