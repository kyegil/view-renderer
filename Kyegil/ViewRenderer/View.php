<?php

namespace Kyegil\ViewRenderer;


use Closure;
use Exception;

class View implements ViewInterface
{
    /** @var ViewFactory */
    protected $viewFactory;

    /** @var string[] */
    protected $templatesFolders = [];

    /** @var string|null */
    protected $template = '';

    /** @var array */
    protected $data = [];

    /** @var array */
    protected $resources = [];

    /** @var AppInterface */
    protected $app;

    /** @var int */
    protected $indentation = 0;

    /** @var ViewInterface|null */
    protected $parent;

    /** @var string */
    protected $array = ViewArray::class;

    /** @var Closure[] */
    protected $instructions = [];

    /** @var Closure[][] */
    protected $instructionsForAscendants = [];

    /**
     * View constructor.
     * @param ViewFactory $viewFactory
     * @param AppInterface $app
     * @param array $data
     */
    public function __construct(
        ViewFactory $viewFactory,
        AppInterface $app,
        array $data = []
    )
    {
        $this->viewFactory = $viewFactory;
        $this->app = $app;
        $this->app->before($this, 'setData', [$data]);
        $this->setData($data);
        $this->app->after($this, 'setData', $this);
        $this->app->before($this, 'prepareData', []);
        $this->prepareData();
        $this->app->after($this, 'prepareData', $this);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        try {
            return $this->render();
        } catch (Exception $e) {
            $message = "{$e->getMessage()}\n";
            $message .= " in " . $e->getFile() . " line " . $e->getLine();
            return $message;
        }
    }

    /**
     * @param string $ascendantClass
     * @param Closure[] $instructions
     * @return View
     */
    public function addInstructionsForAscendant(string $ascendantClass, array $instructions): View
    {
        settype($this->instructionsForAscendants[$ascendantClass], 'array');
        $this->instructionsForAscendants[$ascendantClass] = array_merge($this->instructionsForAscendants[$ascendantClass], $instructions);
        return $this;
    }

    /**
     * @return string[]
     */
    public function getTemplatesFolders()
    {
        return $this->templatesFolders;
    }

    /**
     * @param string $template
     * @return View
     */
    public function setTemplate(string $template): View
    {
        $this->template = $template;
        return $this;
    }

    /**
     * @return string
     */
    public function getTemplate(): string
    {
        return $this->template;
    }

    /**
     * Gets the data for a given key,
     * or all data as associative array if no key is given
     *
     * @param string|null $data
     * @return array|mixed
     */
    public function getData(string $data = null)
    {
        if(isset($data)) {
            return $this->data[$data] ?? null;
        }
        return $this->data;
    }

    /**
     * Set or overwrite a data value
     *
     * Sets a data value if a string key is given,
     * or all data if an associative array is given as first and only parameter
     *
     * @param array|string $keyOrDataSet
     * @param mixed $value
     * @return $this
     */
    public function setData($keyOrDataSet, $value = null)
    {
        if(is_array($keyOrDataSet)) {
            foreach($keyOrDataSet as $key => $data) {
                $this->setData($key, $data);
            }
        }
        elseif(is_string($keyOrDataSet)) {
            $value = is_array($value) ? new ViewArray($value): $value;
            $this->data[$keyOrDataSet] = $value;
            $this->setParentToChild($value);
        }
        return $this;
    }

    /**
     * Add values to the existing values for a given key
     *
     * @param $key
     * @param $value
     * @return $this
     */
    public function addData($key, $value)
    {
        $data = isset($this->data[$key]) ? $this->data[$key] : [];
        if(!is_a($data, ViewArray::class)
        ) {
            /** @var ViewArray $data */
            $data = new $this->array([$data]);
        }
        if(is_array($value)) {
            $data->addItems($value);
        }
        else {
            $data->addItem($value);
        }
        return $this->setData($key, $data);
    }

    /**
     * @param string $resource
     * @return mixed
     */
    public function getResource(string $resource)
    {
        return $this->resources[$resource] ?? null;
    }

    /**
     * @param string $resource
     * @param mixed $value
     * @return $this
     */
    public function setResource(string $resource, $value = null)
    {
        $this->resources[$resource] = $value;
        return $this;
    }

    /**
     * Check if the template exists in any of the templates folders
     *
     * @param string $template
     * @return bool
     */
    public function templateExists(string $template): bool
    {
        foreach($this->getTemplatesFolders() as $templatesFolder) {
            if(file_exists("{$templatesFolder}{$this->template}.php")) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return string
     * @throws Exception
     */
    public function render()
    {
        $this->app->before($this, __FUNCTION__, []);

        extract($this->data, EXTR_SKIP);
        ob_start();
        foreach($this->getTemplatesFolders() as $templatesFolder) {
            if(file_exists("{$templatesFolder}{$this->template}.php")) {
                include("{$templatesFolder}{$this->template}.php");
                break;
            }
        }
        $result = ob_get_clean();
        if($this->getIndentation()) {
            $result = $this->indentContent($result);
        }
        return $this->app->after($this, __FUNCTION__, $result);
    }

    /**
     * Set the templates path folders with the highest priority first
     *
     * @param string[] $templatesFolders
     * @return View
     */
    public function setTemplatesFolders(array $templatesFolders): View
    {
        list('templatesFolders' => $templatesFolders)
            = $this->app->before($this, __FUNCTION__, [
            'templatesFolders' => $templatesFolders
        ]);
        $this->templatesFolders = $templatesFolders;
        return $this->app->after($this, __FUNCTION__, $this);
    }

    /**
     * Add a template prioritised templates path
     *
     * @param string $templatesFolder
     * @return View
     */
    public function addTemplatesFolder(string $templatesFolder): View
    {
        list('templatesFolder' => $templatesFolder)
            = $this->app->before($this, __FUNCTION__, [
            'templatesFolder' => $templatesFolder
        ]);
        array_unshift($this->templatesFolders, $templatesFolder);
        return $this->app->after($this, __FUNCTION__, $this);
    }

    /**
     * @param int $indentation
     * @return View
     */
    public function setIndentation(int $indentation): View
    {
        $this->indentation = $indentation;
        return $this;
    }

    /**
     * @param int $increase
     * @return View
     */
    public function increaseIndentation(int $increase = 1): View
    {
        $this->indentation += $increase;
        return $this;
    }

    /**
     * @param int $decrease
     * @return View
     */
    public function decreaseIndentation(int $decrease = 1): View
    {
        if($this->indentation >= $decrease) {
            $this->indentation -= $decrease;
        }
        return $this;
    }

    /**
     * @return int
     */
    public function getIndentation(): int
    {
        return $this->indentation;
    }

    /**
     * @param ViewInterface|null $parent
     * @return View
     */
    public function setParent(?ViewInterface $parent = null): ViewInterface
    {
        list('parent' => $parent)
            = $this->app->before($this, __FUNCTION__, [
            'parent' => $parent
        ]);
        if(empty($this->parent)
            || empty($parent)
            || get_class($this->parent) != get_class($parent)
        ) {
            $this->parent = $parent;
            if($parent) {
                $this->passInstructionsToParent($parent);
            }
        }
        return $this->app->after($this, __FUNCTION__, $this);
    }

    /**
     * @return ViewInterface|null
     */
    public function getParent(): ?ViewInterface
    {
        $this->app->before($this, __FUNCTION__, []);
        return $this->app->after($this, __FUNCTION__, $this->parent);
    }

    /**
     * @param bool $increaseIndentation
     * @return $this
     */
    protected function setParentToChildren()
    {
        foreach($this->getData() as &$child) {
            $this->setParentToChild($child);
        }
        return $this;
    }

    /**
     * @param $child
     * @return $this
     */
    protected function setParentToChild($child) {
        if($child instanceof ViewInterface) {
            $child->setParent($this);
        }
        return $this;
    }

    /**
     * @param string $content
     * @return string
     */
    protected function indentContent(string $content): string
    {
        $indentation = $this->getIndentation();
        $indentationString = str_repeat('    ', $indentation);
        $lines = explode("\n", $content);
        $result = $indentationString . implode("\n". $indentationString, $lines);
        return $result;
    }

    /**
     * @return $this
     */
    protected function prepareData()
    {
        foreach($this->instructions as $instruction) {
            $this->executeInstruction($instruction);
        }
        return $this;
    }

    /**
     * @param string $property
     * @return bool
     */
    public function hasData(string $property) {
        return isset($this->data[$property]);
    }

    /**
     * @param array|string $keyOrDataSet
     * @param null $value
     * @return $this
     */
    public function setDataIfNotSet($keyOrDataSet, $value = null)
    {
        list('keyOrDataSet' => $keyOrDataSet, 'value' => $value)
            = $this->app->before($this, __FUNCTION__, [
            'keyOrDataSet' => $keyOrDataSet, 'value' => $value
        ]);
        if(is_array($keyOrDataSet)) {
            foreach($keyOrDataSet as $key => $data) {
                if(!$this->hasData($key)) {
                    $this->setData($key, $data);
                }
            }
        }
        elseif(is_string($keyOrDataSet)) {
            if(!$this->hasData($keyOrDataSet)) {
                $value = is_array($value) ? new ViewArray($value): $value;
                $this->setData($keyOrDataSet, $value);
            }
        }
        return $this->app->after($this, __FUNCTION__, $this);
    }

    /**
     * Returns the first ascendant that is of the requested class,
     * or null if it does not exist
     *
     * @param string $class
     * @return ViewInterface|null
     */
    public function getFirstAscendantByClass(string $class) {
        $object = $this;
        while(
            is_a($object->getParent(), ViewInterface::class, true)
            && !is_a($object->getParent(), $class, true)
        ) {
            $object = $object->getParent();
        }
        return $object->getParent();
    }

    /**
     * @param string $class
     * @param false $recursive
     * @return View[]
     */
    public function getDescendantsByClass(string $class, $recursive = false): array
    {
        $result = [];
        foreach ($this->data as $descendant) {
            if($descendant instanceof View) {
                if(is_a($descendant, $class, true)) {
                    $result[] = $descendant;
                }
                if($recursive) {
                    $result = array_merge($result, $descendant->getDescendantsByClass($class, $recursive));
                }
            }
            if($descendant instanceof ViewArray) {
                $result = array_merge($result, $descendant->getDescendantsByClass($class, $recursive));
            }
        }
        return $result;
    }

    /**
     * Pass a callable for execution by a particular ascendant (identified by class)
     * The executing ascendant instance will be passes as the only argument to the callable
     * The keyword $this is also available in the function body,
     * and represents the object which passed the callable
     *
     * @param string $class
     * @param Closure $instruction
     * @return $this
     */
    public function tellAscendant(string $class, Closure $instruction)
    {
        $ascendant = $this->getFirstAscendantByClass($class);
        if($ascendant) {
            call_user_func($instruction, $ascendant);
        }
        else{
            $this->addInstructionsForAscendant($class, [$instruction]);
        }
        return $this;
    }

    /**
     * @param ViewInterface $parent
     * @return $this
     */
    protected function passInstructionsToParent(ViewInterface $parent)
    {
        /**
         * @var string $class
         * @var Closure[] $instructions
         */
        foreach($this->instructionsForAscendants as $class => $instructions) {
            if($parent instanceof $class) {
                $parent->addInstructions($instructions);
            }
            else {
                $parent->addInstructionsForAscendant($class, $instructions);
            }
        }
        return $this;
    }

    /**
     * @param Closure[] $instructions
     * @return View
     */
    public function addInstructions(array $instructions) {
        $this->instructions = array_merge($this->instructions, $instructions);
        return $this;
    }

    /**
     * @param Closure $instruction
     * @return $this
     */
    public function executeInstruction(Closure $instruction)
    {
        call_user_func($instruction, $this);
        return $this;
    }

    /**
     * @param string $class
     * @param string $template
     * @param bool $recursive
     * @return View
     */
    public function setTemplateOnChildClass(string $class, string $template, $recursive = false): View
    {
        foreach($this->getDescendantsByClass($class, $recursive) as $descendant) {
            $descendant->setTemplate($template);
        }
        return $this;
    }

    /**
     * @return AppInterface
     */
    public function getApp(): AppInterface
    {
        return $this->app;
    }
}