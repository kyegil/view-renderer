<?php
/**
 * Part of ViewRenderer
 * Created by Kyegil
 * Date: 29/06/2020
 * Time: 13:22
 */

namespace Kyegil\ViewRenderer;


interface AppInterface
{
    /**
     * @param $subject
     * @param string $method
     * @param array $args
     * @return array
     */
    public function before($subject, string $method, array $args);

    /**
     * @param $subject
     * @param string $method
     * @param mixed $result
     * @return mixed
     */
    public function after($subject, string $method, $result);

    /**
     * @return ViewFactory
     */
    public function getViewFactory(): ViewFactory;
}