<?php

namespace Kyegil\ViewRenderer;


use Closure;

class ViewArray implements ViewInterface, \Iterator
{
    /**
     * @var int
     */
    private $position = 0;

    /**
     * @var array
     */
    protected $items = [];

    /**
     * @var string
     */
    protected $glue = '';

    /**
     * @var string
     */
    protected $prefix = '';

    /**
     * @var string
     */
    protected $suffix = '';

    /**
     * @var ViewInterface|null
     */
    protected $parent;

    /** @var Closure[] */
    protected $instructions = [];

    /** @var Closure[][] */
    protected $instructionsForAscendants = [];

    /**
     * ViewArray constructor.
     * @param array $items
     */
    public function __construct(array $items = [])
    {
        $this->setItems(array_values($items));
    }

    /**
     * @param $name
     * @param $arguments
     */
    public function __call($name, $arguments)
    {
        foreach ($this->items as $item) {
            if(method_exists($item, $name)) {
                call_user_func_array([$item, $name], $arguments);
            }
        }
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->render();
    }

    /**
     * @param string $ascendantClass
     * @param Closure[] $instructions
     * @return ViewArray
     */
    public function addInstructionsForAscendant(string $ascendantClass, array $instructions): ViewArray
    {
        settype($this->instructionsForAscendants[$ascendantClass], 'array');
        $this->instructionsForAscendants[$ascendantClass] = array_merge($this->instructionsForAscendants[$ascendantClass], $instructions);
        return $this;
    }

    /**
     * @return string
     */
    public function render(): string
    {
        return $this->prefix . implode($this->glue, $this->getItems()) . $this->suffix;
    }

    /**
     * @return mixed
     */
    public function current()
    {
        return $this->items[$this->position];
    }

    /**
     *
     */
    public function next()
    {
        ++$this->position;
    }

    /**
     * @return mixed
     */
    public function key()
    {
        return $this->position;
    }

    /**
     * @return bool
     */
    public function valid()
    {
        return isset($this->items[$this->position]);
    }

    /**
     *
     */
    public function rewind()
    {
        $this->position = 0;
    }

    /**
     * @param array $items
     * @return ViewArray
     */
    public function setItems(array $items): ViewArray
    {
        $this->items = $items;
        return $this->setParentToChildren();
    }

    /**
     * @param $item
     * @return $this
     */
    public function addItem($item)
    {
        $this->items[] = $item;
        return $this->setParentToChild($item);
    }

    /**
     * @param array $items
     * @return $this
     */
    public function addItems(array $items)
    {
        $this->items = array_merge($this->items, array_values($items));
        return $this;
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param string $glue
     * @return ViewArray
     */
    public function setGlue(string $glue): ViewArray
    {
        $this->glue = $glue;
        return $this;
    }

    /**
     * @return string
     */
    public function getGlue(): string
    {
        return $this->glue;
    }

    /**
     * @param ViewInterface|null $parent
     * @return View
     */
    public function setParent(?ViewInterface $parent = null): ViewInterface
    {
        if(empty($this->parent)
            || empty($parent)
            || get_class($this->parent) != get_class($parent)
        ) {
            $this->parent = $parent;
            if($parent) {
                $this->passInstructionsToParent($parent);
            }
        }
        return $this;
    }

    /**
     * @return ViewInterface|null
     */
    public function getParent(): ?ViewInterface
    {
        return $this->parent;
    }

    /**
     * Set the array prefix
     *
     * @param string $prefix
     * @return ViewArray
     */
    public function setPrefix(string $prefix): ViewArray
    {
        $this->prefix = $prefix;
        return $this;
    }

    /**
     * Set the array suffix
     *
     * @param string $suffix
     * @return ViewArray
     */
    public function setSuffix(string $suffix): ViewArray
    {
        $this->suffix = $suffix;
        return $this;
    }

    /**
     * @return $this
     */
    protected function setParentToChildren()
    {
        foreach($this->getItems() as &$child) {
            $this->setParentToChild($child);
        }
        return $this;
    }

    /**
     * @param $child
     * @return $this
     */
    protected function setParentToChild($child) {
        if($child instanceof ViewInterface) {
            $child->setParent($this);
        }
        return $this;
    }

    /**
     * @param string $class
     * @param false $recursive
     * @return View[]
     */
    public function getDescendantsByClass(string $class, $recursive = false): array
    {
        $result = [];
        foreach ($this->items as $descendant) {
            if($descendant instanceof View) {
                if(is_a($descendant, $class, true)) {
                    $result[] = $descendant;
                }
                if($recursive) {
                    $result = array_merge($result, $descendant->getDescendantsByClass($class, $recursive));
                }
            }
            if($descendant instanceof ViewArray) {
                $result = array_merge($result, $descendant->getDescendantsByClass($class, $recursive));
            }
        }
        return $result;
    }

    /**
     * Pass a callable for execution by a particular ascendant (identified by class)
     * The executing ascendant instance will be passes as the only argument to the callable
     * The keyword $this is also available in the function body,
     * and represents the object which passed the callable
     *
     * @param string $class
     * @param Closure $instruction
     * @return $this
     */
    public function tellAscendant(string $class, \Closure $instruction)
    {
        $ascendant = $this->getFirstAscendantByClass($class);
        if($ascendant) {
            call_user_func($instruction, $ascendant);
        }
        else{
            $this->addInstructionsForAscendant($class, [$instruction]);
        }
        return $this;
    }

    /**
     * @param ViewInterface $parent
     * @return $this
     */
    protected function passInstructionsToParent(ViewInterface $parent)
    {
        /**
         * @var string $class
         * @var Closure[] $instructions
         */
        foreach($this->instructionsForAscendants as $class => $instructions) {
            if($parent instanceof $class) {
                $parent->addInstructions($instructions);
            }
            else {
                $parent->addInstructionsForAscendant($class, $instructions);
            }
        }
        return $this;
    }

    /**
     * @param Closure[] $instructions
     * @return ViewArray
     */
    public function addInstructions(array $instructions) {
        $this->instructions = array_merge($this->instructions, $instructions);
        return $this;
    }

    /**
     * @param Closure $instruction
     * @return $this
     */
    public function executeInstruction(Closure $instruction)
    {
        call_user_func($instruction, $this);
        return $this;
    }

    /**
     * @param string $class
     * @param string $template
     * @param bool $recursive
     * @return ViewArray
     */
    public function setTemplateOnChildClass(string $class, string $template, $recursive = false): ViewArray
    {
        foreach($this->getDescendantsByClass($class, $recursive) as $descendant) {
            $descendant->setTemplate($template);
        }
        return $this;
    }
}