<?php
/**
 * Part of ViewRenderer
 * Created by Kyegil
 * Date: 29/06/2020
 * Time: 11:51
 */

namespace Kyegil\ViewRenderer;


use Exception;

class ViewFactory
{
    /**
     * @var string
     */
    protected $baseNameSpace = '';
    /**
     * @var string[]
     */
    protected $templatesFolderPaths;
    /**
     * @var AppInterface
     */
    protected $app;

    /**
     * @var string
     */
    protected $defaultViewClass = View::class;

    /**
     * ViewFactory constructor.
     * @param string[] $templatesFolderPaths
     */
    public function __construct(
        AppInterface $app,
        array $templatesFolderPaths
    )
    {
        $this->templatesFolderPaths = $templatesFolderPaths;
        $this->app = $app;
    }

    /**
     * @param string $view
     * @param $data
     * @return View
     */
    public function createView(string $viewClass, $data): ViewInterface
    {
        list('viewClass' => $viewClass, 'data' => $data)
            = $this->app->before($this, __FUNCTION__, [
            'viewClass' => $viewClass, 'data' => $data
        ]);
        if(class_exists($viewClass) && is_a($viewClass, ViewInterface::class, true)) {
            /** @var ViewInterface $view */
            $view = new $viewClass($this, $this->app, $data);
            $view->setTemplatesFolders($this->templatesFolderPaths);
        }
        else {
            $defaultViewClass = $this->getDefaultViewClass();
            /** @var ViewInterface $view */
            $view = new $defaultViewClass($this, $this->app, $data);
            $view->setTemplatesFolders($this->templatesFolderPaths);
            $template = $this->resolveTemplate($viewClass);
            $view->setTemplate($template);
        }
        return $this->app->after($this, __FUNCTION__, $view);
    }

    /**
     * @param string[] $templatesFolderPaths
     * @return ViewFactory
     */
    public function setTemplatesFolderPaths(array $templatesFolderPaths): ViewFactory
    {
        $this->templatesFolderPaths = $templatesFolderPaths;
        return $this;
    }

    /**
     * @param string $templatesFolderPath
     * @return ViewFactory
     */
    public function addTemplatesFolderPath(string $templatesFolderPath): ViewFactory
    {
        array_unshift($this->templatesFolderPaths, $templatesFolderPath);
        return $this;
    }

    /**
     * @return string[]
     */
    public function getTemplatesFolderPaths(): array
    {
        return $this->templatesFolderPaths;
    }

    /**
     * Get the View class for this template
     *
     * @param string $template
     * @return string
     */
    public function getViewClass(string $template)
    {
        $class = trim(str_replace('/', '\\', $template), '\\');
        $class = $this->getBaseNameSpace() . '\\' . $class;
        if(class_exists($class ) && is_a($class, ViewInterface::class)) {
            return $class;
        }
        else {
            return View::class;
        }
    }

    /**
     * @param string $baseNameSpace
     * @return ViewFactory
     */
    public function setBaseNameSpace(string $baseNameSpace): ViewFactory
    {
        $this->baseNameSpace = $baseNameSpace;
        return $this;
    }

    /**
     * @return string
     */
    public function getBaseNameSpace(): string
    {
        return $this->baseNameSpace;
    }

    /**
     * @param string $viewClass
     * @return bool|string|string[]
     * @throws Exception
     */
    public function resolveTemplate(string $viewClass)
    {
        $path = str_replace('\\', '/', $viewClass);
        $template = str_replace($this->getBaseNameSpace(), '', $path);
        foreach ($this->getTemplatesFolderPaths() as $templatesFolderPath) {
            if(file_exists($this->getTemplatesFolderPaths() . $template . '.php')) {
                return $template;
            }
        }
        throw new Exception('Cannot resolve the view template: ' . $template . '.php');
    }

    /**
     * @return string
     */
    public function getDefaultViewClass(): string
    {
        return $this->defaultViewClass;
    }

    /**
     * @param string $defaultViewClass
     * @return ViewFactory
     */
    public function setDefaultViewClass(string $defaultViewClass): ViewFactory
    {
        $this->defaultViewClass = $defaultViewClass;
        return $this;
    }
}