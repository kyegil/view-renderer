# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]

-

## [1.0.8] - 2024-07-24

### Added

- Added instructions transfer to view arrays

## [1.0.6] - 2022-12-12

### Added

- Added View::getApp() method

## [1.0.5] - 2022-12-09

### Added

- Added this changelog
- Added events dispatches.